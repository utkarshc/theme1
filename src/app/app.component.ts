import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'theme';
  f() {
    document.documentElement.style.setProperty(
      '--ravi',
      '"Times New Roman", Times, serif'
    );
    console.log('hello');
  }
  g() {
    document.documentElement.style.setProperty('--ravi', 'sans-serif');
    console.log('hello');
  }
  h() {
    document.documentElement.style.setProperty(
      '--ravi',
      '"Lucida Console", "Courier New", monospace'
    );
    console.log('hello');
  }
}
document.documentElement.style.setProperty('--wbColor', 'green');
document.documentElement.style.setProperty('--ubColor', 'yellow');
